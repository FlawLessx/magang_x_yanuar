import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hello_world/core/model/book_model.dart';
import 'package:hello_world/screen/widget/custom_button.dart';

import 'book_dialog.dart';

class BookItem extends StatelessWidget {
  final BookModel bookModel;
  final Function() editFunction;
  final Function() deleteFunction;

  BookItem(
      {@required this.bookModel,
      @required this.editFunction,
      @required this.deleteFunction});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey.withOpacity(0.5), width: 2)),
      child: Row(
        children: [
          Image.file(
            File(bookModel.gambar),
            height: 100,
            width: 80,
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                bookModel.judulBuku,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 18),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Center(
                  child: Text(
                    bookModel.status,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      width: 80,
                      child:
                          CustomButton(text: 'Edit', onPressed: editFunction)),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                      width: 80,
                      child: CustomButton(
                          text: 'Hapus', onPressed: deleteFunction)),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
