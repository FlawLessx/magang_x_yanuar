import 'package:flutter/widgets.dart';

class FormModel {
  final TextEditingController controller;
  final String labelText;
  final Function() function;

  FormModel(
      {@required this.controller, @required this.labelText, this.function});
}
