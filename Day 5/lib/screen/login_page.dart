import 'package:flutter/material.dart';
import 'package:hello_world/core/database/db_helper.dart';
import 'package:hello_world/screen/home_page.dart';
import 'package:hello_world/screen/register_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'widget/custom_button.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  final DBHelper _dbHelper = DBHelper();
  bool isNotFound = false;

  _getAccount() async {
    final tempData =
        await _dbHelper.getUser(emailController.text, passwordController.text);
    if (tempData == null) {
      isNotFound = true;
      setState(() {});
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setInt('userID', tempData.id);

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => MyHomePage(
                    title: 'Day 4',
                  )),
          (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Page'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: emailController,
              decoration: InputDecoration(hintText: 'Insert Email'),
            ),
            TextField(
              controller: passwordController,
              decoration: InputDecoration(hintText: 'Insert Password'),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Visibility(
                  visible: isNotFound,
                  child: Text(
                    'Account not found',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
                InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegisterPage()));
                    },
                    child: Text('Register account'))
              ],
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(text: 'Login', onPressed: _getAccount),
              ],
            )
          ],
        ),
      ),
    );
  }
}
