import 'package:flutter/material.dart';
import 'package:hello_world/core/database/db_helper.dart';
import 'package:hello_world/core/model/book_model.dart';
import 'package:hello_world/screen/widget/book_dialog.dart';
import 'package:hello_world/screen/widget/book_item.dart';

class ListBukuPage extends StatefulWidget {
  @override
  _ListBukuPageState createState() => _ListBukuPageState();
}

class _ListBukuPageState extends State<ListBukuPage> {
  DBHelper _dbHelper = DBHelper();
  List<BookModel> listBook = [];

  _getBook() async {
    listBook = await _dbHelper.getBook();
    setState(() {});
  }

  _deleteBook(int index, int bookId) {
    _dbHelper.deleteBook(bookId);
    listBook.removeAt(index);
    setState(() {});
  }

  @override
  void initState() {
    _getBook();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buku Saya Page'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(context: context, builder: (context) => BookDialog());
        },
        child: Icon(Icons.add),
      ),
      body: ListView.builder(
          padding: EdgeInsets.all(20),
          itemCount: listBook.length,
          itemBuilder: (context, index) => Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: BookItem(
                  role: "Pemilik",
                  isBorrowed: false,
                  bookModel: listBook[index],
                  editFunction: () async {
                    await showDialog(
                        context: context,
                        builder: (context) => BookDialog(
                              bookModel: listBook[index],
                              func: () {
                                Navigator.pop(context);
                              },
                            ));
                    _getBook();
                  },
                  deleteFunction: () {
                    _deleteBook(index, listBook[index].id);
                  },
                ),
              )),
    );
  }
}
