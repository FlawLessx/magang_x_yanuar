import 'package:flutter/material.dart';
import 'package:hello_world/core/database/db_helper.dart';
import 'package:hello_world/core/model/book_model.dart';
import 'package:hello_world/screen/widget/book_item.dart';
import 'package:hello_world/screen/widget/description_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PeminjamanPage extends StatefulWidget {
  @override
  _PeminjamanPageState createState() => _PeminjamanPageState();
}

class _PeminjamanPageState extends State<PeminjamanPage> {
  DBHelper _dbHelper = DBHelper();
  List<BookModel> listBook = [];
  int id;

  _getBook() async {
    listBook = await _dbHelper.getBook();
    setState(() {});
  }

  _getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int tempId = prefs.getInt('userID');
    print("temp: " + tempId.toString());

    if (tempId != null) {
      final user = await _dbHelper.getUserWithId(tempId);
      if (user != null) {
        id = user.id;
        setState(() {});
      }
    }
  }

  _updateBook(BookModel bookModel, int peminjamId) async {
    BookModel book = BookModel(
        id: bookModel.id,
        genreId: bookModel.genreId,
        harga: bookModel.harga,
        pemilik: bookModel.pemilik,
        peminjam: peminjamId,
        publish: bookModel.publish,
        gambar: bookModel.gambar,
        judulBuku: bookModel.judulBuku,
        status: bookModel.status);
    await _dbHelper.updateBook(book);
    setState(() {});
    _getBook();
  }

  @override
  void initState() {
    _getBook();
    _getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Peminjaman Page")),
      body: ListView.builder(
          itemCount: listBook.length,
          padding: EdgeInsets.all(20),
          itemBuilder: (context, index) => Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: BookItem(
                    role: "Peminjam",
                    bookModel: listBook[index],
                    editFunction: () {
                      showDialog(
                          context: context,
                          builder: (context) => DescriptionDialog(
                              text: listBook[index].deskripsi ??
                                  "Tidak ada deskripsi"));
                    },
                    deleteFunction: () {
                      _updateBook(listBook[index], id);
                    }),
              )),
    );
  }
}
