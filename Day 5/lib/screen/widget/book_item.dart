import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hello_world/core/database/db_helper.dart';
import 'package:hello_world/core/model/book_model.dart';
import 'package:hello_world/screen/widget/custom_button.dart';

class BookItem extends StatefulWidget {
  final String role;
  final BookModel bookModel;
  final Function() editFunction;
  final Function() deleteFunction;
  final bool isBorrowed;

  BookItem({
    @required this.role,
    @required this.bookModel,
    @required this.editFunction,
    @required this.deleteFunction,
    this.isBorrowed,
  });

  @override
  _BookItemState createState() => _BookItemState();
}

class _BookItemState extends State<BookItem> {
  final DBHelper _dbHelper = DBHelper();
  String peminjam = '';

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  _getUser() async {
    final user = await _dbHelper.getUserWithId(widget.bookModel.peminjam);
    if (user != null) {
      peminjam = user.nama;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey.withOpacity(0.5), width: 2)),
      child: Row(
        children: [
          widget.bookModel.gambar != null
              ? Image.file(
                  File(widget.bookModel.gambar),
                  height: 100,
                  width: 80,
                )
              : Container(
                  width: 80,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add_a_photo,
                        color: Colors.black,
                      ),
                      Text('Tambah Gambar', textAlign: TextAlign.center),
                    ],
                  ),
                ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.bookModel.judulBuku,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 18),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Center(
                  child: Text(
                    widget.role == "Peminjam"
                        ? widget.bookModel.harga
                        : widget.bookModel.status,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              if (widget.bookModel.peminjam != null && widget.isBorrowed ||
                  widget.bookModel.peminjam != null && widget.role == "Pemilik")
                Text("Peminjam: $peminjam"),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      width: 80,
                      child: CustomButton(
                          text: widget.role == "Pemilik" ? 'Edit' : 'Deskripsi',
                          onPressed: widget.editFunction)),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                      width: 80,
                      child: CustomButton(
                          text: widget.role == "Pemilik"
                              ? 'Hapus'
                              : widget.isBorrowed == true
                                  ? "Dipinjam"
                                  : "Pinjam",
                          onPressed: widget.deleteFunction)),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
