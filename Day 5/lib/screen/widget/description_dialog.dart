import 'package:flutter/material.dart';

class DescriptionDialog extends StatelessWidget {
  final String text;

  DescriptionDialog({@required this.text});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text(text),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('Close'))
      ],
    );
  }
}
