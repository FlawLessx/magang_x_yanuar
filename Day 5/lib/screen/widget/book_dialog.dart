import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hello_world/core/database/db_helper.dart';
import 'package:hello_world/core/model/book_model.dart';
import 'package:hello_world/screen/widget/custom_button.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookDialog extends StatefulWidget {
  final BookModel bookModel;
  final Function() func;

  BookDialog({this.bookModel, this.func});

  @override
  _BookDialogState createState() => _BookDialogState();
}

class _BookDialogState extends State<BookDialog> {
  final DBHelper _dbHelper = DBHelper();
  final TextEditingController judulController = TextEditingController();
  final TextEditingController hargaController = TextEditingController();
  final TextEditingController deskripsiController = TextEditingController();
  String selectedStatus = 'Not Published';
  List<String> listStatusPublish = ['Published', 'Not Published'];
  String imagePath;
  final _picker = ImagePicker();
  int id;

  @override
  void initState() {
    super.initState();
    _getUser();
    _assignData();
  }

  _getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int tempId = prefs.getInt('userID');

    if (id != null) {
      final user = await _dbHelper.getUserWithId(tempId);
      if (user != null) {
        id = user.id;
        setState(() {});
      }
    }
  }

  _addBook() async {
    BookModel book = BookModel(
        gambar: imagePath,
        judulBuku: judulController.text,
        status: selectedStatus,
        harga: hargaController.text,
        pemilik: id,
        deskripsi: deskripsiController.text);
    await _dbHelper.insertBook(book);
    Navigator.pop(context);
  }

  _updateBook() async {
    BookModel book = BookModel(
        id: widget.bookModel.id,
        genreId: widget.bookModel.genreId,
        harga: widget.bookModel.harga,
        pemilik: widget.bookModel.pemilik,
        peminjam: widget.bookModel.peminjam,
        publish: widget.bookModel.publish,
        gambar: imagePath,
        judulBuku: judulController.text,
        deskripsi: deskripsiController.text,
        status: selectedStatus);
    await _dbHelper.updateBook(book);
    widget.func.call();
  }

  _addImage() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      imagePath = pickedFile.path;
      setState(() {});
    }
  }

  _assignData() {
    if (widget.bookModel != null) {
      imagePath = widget.bookModel.gambar;
      judulController.text = widget.bookModel.judulBuku;
      hargaController.text = widget.bookModel.harga;
      deskripsiController.text = widget.bookModel.deskripsi;
      selectedStatus = widget.bookModel.status;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.all(20),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () async {
                      _addImage();
                    },
                    child: Container(
                      color: imagePath != null
                          ? Colors.transparent
                          : Colors.grey.withOpacity(0.3),
                      height: 100,
                      width: 100,
                      child: imagePath != null
                          ? Image.file(File(imagePath))
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.add_a_photo,
                                  color: Colors.black,
                                ),
                                Text('Tambah Gambar',
                                    textAlign: TextAlign.center),
                              ],
                            ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10),
              TextField(
                controller: judulController,
                decoration: InputDecoration(hintText: "Judul Buku"),
              ),
              TextField(
                controller: hargaController,
                decoration: InputDecoration(hintText: "Harga Buku"),
              ),
              TextField(
                controller: deskripsiController,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: InputDecoration(
                  hintText: "Deskripsi",
                ),
              ),
              SizedBox(height: 10),
              DropdownButton(
                hint: Text("Select Status"),
                value: selectedStatus,
                items: listStatusPublish.map((value) {
                  return DropdownMenuItem(
                    child: Text(value),
                    value: value,
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    selectedStatus = value;
                  });
                },
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomButton(
                      text: widget.bookModel != null
                          ? "Update Buku"
                          : "Tambah Buku",
                      onPressed: () {
                        widget.bookModel != null ? _updateBook() : _addBook();
                      })
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
