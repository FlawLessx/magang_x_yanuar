import 'package:flutter/material.dart';
import 'package:hello_world/core/database/db_helper.dart';
import 'package:hello_world/core/model/drawer_model.dart';
import 'package:hello_world/core/model/user_model.dart';
import 'package:hello_world/screen/list_buku_page.dart';
import 'package:hello_world/screen/login_page.dart';
import 'package:hello_world/screen/peminjaman_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  List<DrawerModel> listDrawerItemUser;
  int selectedIndex = 0;

  List<DrawerModel> listDrawerItemOwner = [];
  UserModel user = UserModel(roles: 'Peminjam');
  DBHelper _dbHelper = DBHelper();

  _signOut() {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
        (Route<dynamic> route) => false);
  }

  _changeSelectedIndex(int index) {
    selectedIndex = index;
    setState(() {});
  }

  Future _getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int id = prefs.getInt('userID');

    if (id != null) {
      user = await _dbHelper.getUserWithId(id);
      setState(() {});
    }
  }

  @override
  void initState() {
    _getUser();
    Future.delayed(Duration(seconds: 1));
    listDrawerItemOwner = [
      DrawerModel(title: 'Home', index: 0, icons: Icons.home_outlined),
      DrawerModel(
          title: 'Buku Saya',
          index: 1,
          icons: Icons.book_outlined,
          function: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ListBukuPage()));
          }),
    ];
    listDrawerItemUser = [
      DrawerModel(title: 'Home', index: 0, icons: Icons.home_outlined),
      DrawerModel(
          title: 'Peminjaman saya',
          index: 2,
          icons: Icons.info_outline,
          function: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => PeminjamanPage()));
          })
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Column(
      children: [
        Expanded(
          child: ListView(
            children: [
              DrawerHeader(
                  child: Image.asset(
                'src/img/book.jpg',
                fit: BoxFit.fill,
              )),
              ListView.separated(
                shrinkWrap: true,
                separatorBuilder: (context, index) => Divider(),
                itemCount: user.roles == 'Peminjam'
                    ? listDrawerItemUser.length
                    : listDrawerItemOwner.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      _changeSelectedIndex(index);
                      if (user.roles == 'Peminjam') {
                        listDrawerItemUser[index].function();
                      } else {
                        listDrawerItemOwner[index].function();
                      }
                    },
                    visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                    leading: Icon(
                      user.roles == 'Peminjam'
                          ? listDrawerItemUser[index].icons
                          : listDrawerItemOwner[index].icons,
                      color:
                          selectedIndex == index ? Colors.blue : Colors.black,
                    ),
                    title: Text(
                      user.roles == 'Peminjam'
                          ? listDrawerItemUser[index].title
                          : listDrawerItemOwner[index].title,
                      style: TextStyle(
                        color:
                            selectedIndex == index ? Colors.blue : Colors.black,
                      ),
                    ),
                    minVerticalPadding: 0,
                  );
                },
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(bottom: 20),
          child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: ListTile(
                onTap: _signOut,
                visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                leading: Icon(
                  Icons.account_box_outlined,
                  color: Colors.black,
                ),
                title: Text('Sign Out'),
                minVerticalPadding: 0,
              )),
        )
      ],
    ));
  }
}
