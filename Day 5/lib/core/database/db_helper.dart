import 'package:hello_world/core/model/book_model.dart';
import 'package:hello_world/core/model/user_model.dart';
import 'package:path/path.dart' as p;
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

import 'db_utils.dart';

class DBHelper {
  static final DBHelper _instance = new DBHelper.internal();
  factory DBHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    var directory = await getApplicationDocumentsDirectory();
    String path = p.join(directory.toString(), "main.db");
    var theDb = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
          create table $tableUser (
          $columnId integer primary key autoincrement,
          $columnNama text not null,
          $columnEmail email not null,
          $columnPassword text not null,
          $columnAlamat text not null,
          $columnNoTelp text not null,
          $columnRoles text not null)
          ''');

      await db.execute('''
          create table $tableRole (
          $columnId integer primary key autoincrement,
          $role text not null)
          ''');

      await db.execute('''
          create table $tableBooks (
          $columnId integer primary key autoincrement,
          $columnGenreId integer,
          $columnJudulBuku text not null,
          $columnHarga text,
          $columnGambar text,
          $columnPemilik int,
          $columnPeminjam int,
          $columnStatus text,
          $columnPublish text,
          $columnDeskripsi deskripsi)
          ''');

      await db.execute('''
          create table $tableGenre (
          $columnId integer primary key autoincrement,
          $columnGenre text not null)
          ''');
    });
    return theDb;
  }

  DBHelper.internal();

  //
  // USER FUNCTION
  //
  Future<UserModel> insertUser(UserModel user) async {
    var dbClient = await db;
    user.id = await dbClient.insert(tableUser, user.toMap());
    return user;
  }

  Future<int> updateUser(UserModel user) async {
    var dbClient = await db;
    return await dbClient.update(tableUser, user.toMap(),
        where: '$columnId = ?', whereArgs: [user.id]);
  }

  Future<UserModel> getUser(String email, String password) async {
    List<UserModel> user = [];
    var dbClient = await db;
    List<Map> maps = await dbClient.query(tableUser,
        columns: [
          columnId,
          columnNama,
          columnEmail,
          columnPassword,
          columnAlamat,
          columnAlamat,
          columnRoles
        ],
        where: '$columnEmail = ? AND $columnPassword = ?',
        whereArgs: [email, password]);
    if (maps.length > 0) {
      maps.forEach((f) {
        user.add(UserModel.fromMap(f));
      });
    }
    return user.length != 0 ? user.first : null;
  }

  Future<UserModel> getUserWithId(int id) async {
    List<UserModel> user = [];
    var dbClient = await db;
    List<Map> maps = await dbClient.query(tableUser,
        columns: [
          columnId,
          columnNama,
          columnEmail,
          columnPassword,
          columnAlamat,
          columnAlamat,
          columnRoles
        ],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      maps.forEach((f) {
        user.add(UserModel.fromMap(f));
      });
    }
    return user.length != 0 ? user.first : null;
  }

  //
  // BOOKS FUNCTION
  //

  Future<BookModel> insertBook(BookModel book) async {
    var dbClient = await db;
    book.id = await dbClient.insert(tableBooks, book.toMap());
    return book;
  }

  Future<int> updateBook(BookModel book) async {
    var dbClient = await db;
    return await dbClient.update(tableBooks, book.toMap(),
        where: '$columnId = ?', whereArgs: [book.id]);
  }

  Future<List<BookModel>> getBook() async {
    List<BookModel> books = [];
    var dbClient = await db;
    List<Map> maps = await dbClient.query(tableBooks, columns: [
      columnId,
      columnGenreId,
      columnJudulBuku,
      columnHarga,
      columnGambar,
      columnPemilik,
      columnPeminjam,
      columnStatus,
      columnPublish,
      columnDeskripsi
    ]);
    if (maps.length > 0) {
      maps.forEach((f) {
        books.add(BookModel.fromMap(f));
      });
    }
    return books;
  }

  Future<int> deleteBook(int id) async {
    var dbClient = await db;
    return await dbClient
        .delete(tableBooks, where: '$columnId = ?', whereArgs: [id]);
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
