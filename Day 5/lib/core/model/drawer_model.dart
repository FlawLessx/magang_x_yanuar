import 'package:flutter/widgets.dart';

class DrawerModel {
  final String title;
  final int index;
  final IconData icons;
  final Function() function;

  DrawerModel(
      {@required this.title,
      @required this.index,
      @required this.icons,
      this.function});
}
