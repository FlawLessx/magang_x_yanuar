import '../database/db_utils.dart';

class BookModel {
  int id;
  int genreId;
  String judulBuku;
  String harga;
  String gambar;
  int pemilik;
  int peminjam;
  String status;
  String publish;
  String deskripsi;

  BookModel(
      {this.id,
      this.genreId,
      this.judulBuku,
      this.harga,
      this.gambar,
      this.pemilik,
      this.peminjam,
      this.status,
      this.publish,
      this.deskripsi});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnId: id,
      columnGenreId: genreId,
      columnJudulBuku: judulBuku,
      columnHarga: harga,
      columnGambar: gambar,
      columnPemilik: pemilik,
      columnPeminjam: peminjam,
      columnStatus: status,
      columnPublish: publish,
      columnDeskripsi: deskripsi
    };
    return map;
  }

  BookModel.fromMap(Map<dynamic, dynamic> map) {
    id = map[columnId];
    genreId = map[columnGenreId];
    judulBuku = map[columnJudulBuku];
    harga = map[columnHarga];
    gambar = map[columnGambar];
    pemilik = map[columnPemilik];
    peminjam = map[columnPeminjam];
    status = map[columnStatus];
    publish = map[columnPublish];
    deskripsi = map[columnDeskripsi];
  }
}
