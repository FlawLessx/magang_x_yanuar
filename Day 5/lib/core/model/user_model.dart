import 'package:hello_world/core/database/db_utils.dart';

class UserModel {
  int id;
  String nama;
  String email;
  String password;
  String alamat;
  String noTelp;
  String roles;

  UserModel({
    this.id,
    this.nama,
    this.email,
    this.password,
    this.alamat,
    this.noTelp,
    this.roles,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnNama: nama,
      columnEmail: email,
      columnPassword: password,
      columnAlamat: alamat,
      columnNoTelp: noTelp,
      columnRoles: roles
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  UserModel.fromMap(Map<dynamic, dynamic> map) {
    id = map[columnId];
    nama = map[columnNama];
    email = map[columnEmail];
    password = map[columnEmail];
    alamat = map[columnPassword];
    noTelp = map[columnNoTelp];
    roles = map[columnRoles];
  }
}
