import java.util.*;
import java.io.*;

class Main {

    public static boolean prior(String former, String latter) {
        if ((former.equals("*") || former.equals("/"))
                && (latter.equals("+") || latter.equals("-") || latter.equals("(")))
            return true;
        else if ((former.equals("+") || former.equals("-")) && latter.equals("("))
            return true;
        else
            return false;
    }

    // Calculate the two numbers.
    public static int figureout(int former, String operator, int latter) {
        if (operator.equals("+"))
            return former + latter;
        else if (operator.equals("-"))
            return former - latter;
        else if (operator.equals("*"))
            return former * latter;
        else if (operator.equals("/"))
            return former / latter;
        else
            return 0;
    }

    // identify the char.
    public static int classifyOperator(String operator) {
        if (operator.equals("0") || operator.equals("1") || operator.equals("2") || operator.equals("3")
                || operator.equals("4") || operator.equals("5") || operator.equals("6") || operator.equals("7")
                || operator.equals("8") || operator.equals("9") || operator.equals("."))
            return 0;
        if (operator.equals("+") || operator.equals("-") || operator.equals("*") || operator.equals("/"))
            return 1;
        if (operator.equals("("))
            return 2;
        if (operator.equals(")"))
            return 3;
        else
            return 4;
    }

    // Calculate the String.
    public static String Calculator(String sentense) {
        Stack<Integer> numbers = new Stack<Integer>();
        Stack<String> operators = new Stack<String>();
        String number = "";

        sentense = "(" + sentense + ")";
        for (int i = 0; i < sentense.length(); i++) {
            if (classifyOperator("" + sentense.charAt(i)) == 0) {
                number = number + sentense.charAt(i);
            } else {
                if (!number.equals(""))
                    numbers.push(Integer.parseInt(number));
                number = "";

                if (classifyOperator("" + sentense.charAt(i)) == 2)
                    operators.push("" + sentense.charAt(i));
                if (classifyOperator("" + sentense.charAt(i)) == 3) {
                    while (!operators.peek().equals("(")) {
                        int latter = numbers.pop();
                        int former = numbers.pop();
                        numbers.push(figureout(former, operators.pop(), latter));
                    }
                    operators.pop();
                }
                if (classifyOperator("" + sentense.charAt(i)) == 1) {

                    if (prior(("" + sentense.charAt(i)), operators.peek()))
                        operators.push("" + sentense.charAt(i));
                    else {
                        int latter = numbers.pop();
                        int former = numbers.pop();
                        do {
                            numbers.push(figureout(former, operators.pop(), latter));
                        } while (!(prior(("" + sentense.charAt(i)), operators.peek())));
                        operators.push("" + sentense.charAt(i));
                    }
                }
            }
        }

        while (!operators.empty()) {
            int latter = numbers.pop();
            int former = numbers.pop();
            numbers.push(figureout(former, operators.pop(), latter));
        }
        return String.format("%d", numbers.pop());
    }

    public static void main(String[] args) {
        // keep this function call here
        Scanner s = new Scanner(System.in);
        System.out.print(Calculator(s.nextLine()));
    }

}