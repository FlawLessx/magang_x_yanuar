final String columnId = '_id';

// User Table
final String tableUser = 'user_table';
final String columnNama = 'nama';
final String columnEmail = 'email';
final String columnPassword = 'password';
final String columnAlamat = 'alamat';
final String columnNoTelp = 'no_telpon';
final String columnRoles = 'roles_id';

// Roles Table
final String tableRole = 'role_table';
final String role = 'role';

// Books Table
final String tableBooks = 'books_table';
final String columnGenreId = 'genre_id';
final String columnJudulBuku = 'judul_buku';
final String columnHarga = 'harga';
final String columnGambar = 'gambar';
final String columnPemilik = 'pemilik_id';
final String columnPeminjam = 'peminjam_id';
final String columnStatus = 'status';
final String columnPublish = 'is_publish';

// Genre Table
final String tableGenre = 'genre_table';
final String columnGenre = 'genre';
