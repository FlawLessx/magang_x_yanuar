import 'package:flutter/widgets.dart';

class FormModel {
  final TextEditingController controller;
  final String labelText;

  FormModel({@required this.controller, @required this.labelText});
}
