import 'package:flutter/material.dart';
import 'package:hello_world/core/database/db_helper.dart';
import 'package:hello_world/core/model/form_model.dart';
import 'package:hello_world/core/model/user_model.dart';
import 'package:hello_world/screen/widget/custom_button.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController namaController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController nomorTelpController = TextEditingController();
  final TextEditingController alamatController = TextEditingController();
  final DBHelper _dbHelper = DBHelper();
  String selectedRole;
  List<String> listRole = ['Pemilik', 'Peminjam'];
  List<FormModel> listForm;

  @override
  void initState() {
    listForm = [
      FormModel(controller: namaController, labelText: 'Nama'),
      FormModel(controller: emailController, labelText: 'Email'),
      FormModel(controller: passwordController, labelText: 'Password'),
      FormModel(controller: nomorTelpController, labelText: 'Nomor Telepon'),
      FormModel(controller: alamatController, labelText: 'Alamat')
    ];
    super.initState();
  }

  Future<void> _saveToDB() async {
    await _dbHelper.insert(UserModel(
        nama: namaController.text,
        email: emailController.text,
        password: passwordController.text,
        noTelp: nomorTelpController.text,
        alamat: alamatController.text,
        roles: selectedRole));

    _clearData();
  }

  _clearData() {
    namaController.text = '';
    emailController.text = '';
    passwordController.text = '';
    nomorTelpController.text = '';
    alamatController.text = '';
    selectedRole = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register Page'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: listForm
                    .map((e) => TextField(
                          controller: e.controller,
                          decoration: InputDecoration(labelText: e.labelText),
                        ))
                    .toList(),
              ),
              DropdownButton(
                hint: Text("Select Role"),
                value: selectedRole,
                items: listRole.map((value) {
                  return DropdownMenuItem(
                    child: Text(value),
                    value: value,
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    selectedRole = value;
                  });
                },
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomButton(text: 'Register', onPressed: _saveToDB),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
